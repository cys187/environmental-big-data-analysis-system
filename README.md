# 基于Hadoop的环境大数据分析系统

#### 介绍
***

#### 软件架构
![系统架构](https://images.gitee.com/uploads/images/2022/0729/211641_278bf98b_9166822.png "屏幕截图.png")


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

### 最终呈现效果
![首页展示](https://images.gitee.com/uploads/images/2022/0729/211833_4cad3116_9166822.png "屏幕截图.png")
![排行榜展示](https://images.gitee.com/uploads/images/2022/0729/211900_f847a7c6_9166822.png "屏幕截图.png")
![数据地图展示](https://images.gitee.com/uploads/images/2022/0729/211924_d8e73d97_9166822.png "屏幕截图.png")

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技
1.个人博客 [blog.gitee.com](https://blog.csdn.net/weixin_44318460)

