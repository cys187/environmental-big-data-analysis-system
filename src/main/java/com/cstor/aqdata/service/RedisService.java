package com.cstor.aqdata.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

@Service
public class RedisService {

    @Autowired
    private StringRedisTemplate redisTemplate;
    
    //创建一个公共缓存字段的方法
    public boolean set(String key, String value) {
        boolean res = false;
        //通过StringRedisTemplate里的方法set键值对
        redisTemplate.opsForValue().set(key, value);
        //判断k是否存在，存在说明缓存成功
        if (redisTemplate.hasKey(key)) {
            res = true;
        }
        return res;
    }

    //创建一个公共方法，来获取主键k对应的value值
    public String get(String key) {
        return redisTemplate.opsForValue().get(key);
    }
}
