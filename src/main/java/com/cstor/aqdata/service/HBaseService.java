package com.cstor.aqdata.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.hadoop.hbase.Cell;
import org.apache.hadoop.hbase.CellUtil;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.util.Bytes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.cstor.aqdata.base.HBaseTemplate;
import com.cstor.aqdata.jpa.StationJPA;
import com.cstor.aqdata.mapper.MariadbMapper;
import com.cstor.aqdata.model.Station;
import com.cstor.aqdata.model.StationData;
import com.cstor.aqdata.util.DateUtil;

/*
 * @Service注解用于类上，标记当前类是一个service类，
 * 加上该注解会将当前类自动注入到spring容器中，不需要再在applicationContext.xml文件定义bean了
 */
@Service
public class HBaseService {
	
	//创建三个私有全局字符串常量分别表明，国控数据站点、城市数据站点、数据表的列族名
    private static final String tableName1 = "station_data";
    private static final String tableName2 = "city_data";
    private static final String familyName = "info";
    
    //注入hBaseTemplate
    @Autowired
    private HBaseTemplate hBaseTemplate;

    @Autowired
    private StationJPA stationJPA;

    @Autowired
    private MariadbMapper mariadbMapper;
    
    //向国控站点插入数据
    public Integer insertStation(List<StationData> stationDatas) {
    	//调用hBaseTemplate方法判断数据表是否存在
        boolean tableExistsFlag = hBaseTemplate.tableExists(tableName1);
        //如果数据表不存在，我们根据表名和列族名创建一个新表
        if (!tableExistsFlag) {
            hBaseTemplate.createTable(tableName1, familyName);
        }
        
        List<Put> putList = new ArrayList<Put>();
        //将stationData数组数据循环添加到put类型数组里
        for (StationData stationData : stationDatas) {
        	//根据stationData里的Rowkey字段创建一个put对象，判断stationData里个各自字段的数据是否为空，不为空则添加到put对象中
            Put put = new Put(stationData.getRowkey().getBytes());
            if (stationData.getStationcode() != null && !stationData.getStationcode().isEmpty()) {
                put.addColumn(familyName.getBytes(), "stationcode".getBytes(), stationData.getStationcode().getBytes());
            }
            if (stationData.getCitycode() != null && !stationData.getCitycode().isEmpty()) {
                put.addColumn(familyName.getBytes(), "citycode".getBytes(), stationData.getCitycode().getBytes());
            }
            if (stationData.getTime() != null && !stationData.getTime().isEmpty()) {
                put.addColumn(familyName.getBytes(), "time".getBytes(), stationData.getTime().getBytes());
            }
            if (stationData.getPM25() != null && !stationData.getPM25().isEmpty()) {
                put.addColumn(familyName.getBytes(), "PM25".getBytes(), stationData.getPM25().getBytes());
            }
            //将put对象添加到put数组中
            putList.add(put);
        }
        hBaseTemplate.putBatch(tableName1, putList);
        
        return stationDatas.size();
    }

    //从国控站点批量查询数据的方法
    public JSONArray queryStation(List<String> rowkeys) {
        JSONArray result = new JSONArray();
        
        List<StationData> stationDatas = qs(rowkeys);
        for (StationData stationData : stationDatas) {
            if (stationData.getRowkey() == null || stationData.getRowkey().isEmpty()) {
                continue;
            }
            JSONObject json = new JSONObject();
            json.put("stationcode", stationData.getStationcode());
            json.put("citycode", stationData.getCitycode());
            json.put("PM25", stationData.getPM25());
            json.put("time", DateUtil.stamp2Str(stationData.getTime()));
            Station station = stationJPA.findByCode(stationData.getStationcode());
            json.put("stationname", station.getName());
            json.put("cityname", station.getCityname());
            json.put("x", station.getX());
            json.put("y", station.getY());
            json.put("flag", station.getFlag());
            json.put("type", "gkd");
            result.add(json);
        }
        
        return result;
    }

    public List<JSONObject> queryCity(List<String> rowkeys) {
        List<JSONObject> cityDatas = new ArrayList<JSONObject>();
        
        List<Get> getList = new ArrayList<Get>();
        for (String rowkey : rowkeys) {
            Get get = new Get(rowkey.getBytes());
            getList.add(get);
        }
        Result[] results = hBaseTemplate.query(tableName2, getList);
        if (results != null && results.length > 0) {
            for (Result result : results) {
                List<Cell> cells = result.listCells();
                if (cells != null && cells.size() > 0) {
                    JSONObject cityData = new JSONObject();
                    for (Cell cell : cells) {
                        String qualifier = Bytes.toString(CellUtil.cloneQualifier(cell));
                        String value = Bytes.toString(CellUtil.cloneValue(cell));
                        
                        if (qualifier.equals("citycode")) {
                            cityData.put("citycode", value);
                        } else if (qualifier.equals("time")) {
                            cityData.put("time", value);
                        } else if (qualifier.equals("PM25")) {
                            cityData.put("PM25", value);
                        }
                        String citycode = cityData.getString("citycode");
                        String cityname = mariadbMapper.getCityname(citycode);
                        cityData.put("cityname", cityname);
                        cityDatas.add(cityData);
                    }
                }
            }
        }
        
        return cityDatas;
    }

    private List<StationData> qs(List<String> rowkeys) {
        // TODO Auto-generated method stub
        List<Get> getList = new ArrayList<Get>();
        for (String rowkey : rowkeys) {
            Get get = new Get(rowkey.getBytes());
            getList.add(get);
        }
        Result[] results = hBaseTemplate.query(tableName1, getList);
        List<StationData> stationDatas = new ArrayList<StationData>();
        if (results != null && results.length > 0) {
            for (Result result : results) {
                StationData stationData = new StationData();
                List<Cell> cells = result.listCells();
                if (cells != null && !cells.isEmpty()) {
                    for (Cell cell : cells) {
                        String rowkey = Bytes.toString(CellUtil.cloneRow(cell));
                        String qualifier = Bytes.toString(CellUtil.cloneQualifier(cell));
                        String value = Bytes.toString(CellUtil.cloneValue(cell));
                        
                        stationData.setRowkey(rowkey);
                        if (qualifier.equals("stationcode")) {
                            stationData.setStationcode(value);
                        } else if (qualifier.equals("citycode")) {
                            stationData.setCitycode(value);
                        } else if (qualifier.equals("time")) {
                            stationData.setTime(value);
                        } else if (qualifier.equals("PM25")) {
                            stationData.setPM25(value);
                        }
                    }
                }
                stationDatas.add(stationData);
            }
        }
        return stationDatas;
    }
}