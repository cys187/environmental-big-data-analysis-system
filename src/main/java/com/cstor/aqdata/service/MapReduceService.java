package com.cstor.aqdata.service;

import java.io.IOException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.MasterNotRunningException;
import org.apache.hadoop.hbase.ZooKeeperConnectionException;
import org.apache.hadoop.hbase.client.HBaseAdmin;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.filter.CompareFilter.CompareOp;
import org.apache.hadoop.hbase.filter.Filter;
import org.apache.hadoop.hbase.filter.RowFilter;
import org.apache.hadoop.hbase.filter.SubstringComparator;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.TableMapReduceUtil;
import org.apache.hadoop.hbase.mapreduce.TableMapper;
import org.apache.hadoop.hbase.mapreduce.TableReducer;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.springframework.stereotype.Service;

@Service
public class MapReduceService {
	//通过静态初始化块，配置Configuration参数
    static Configuration configuration = null;
    static {
        configuration = HBaseConfiguration.create();
        configuration.set("hbase.zookeeper.quorum", "master");
        configuration.set("hbase.zookeeper.property.clientPort", "2181");
    }
    //创建三个私有的全局常量，inTable用于计算的来源表明，ouTable存储计算结果的表明，familyName数据表的列族名
    private static final String inTable = "station_data";
    private static final String outTable = "city_data";
    private static final String familyName = "info";
    //新建一个CityDataMapper类用来从hbase中查询数据，继承TableMapper类（专为从Hbase读取数据而定义的抽象类）
    public static class CityDataMapper extends TableMapper<Text, IntWritable> {
        public Text k = new Text();
        public IntWritable v = new IntWritable();
        
        //重写父类的方法
        @Override
        protected void map(ImmutableBytesWritable key, Result value, Context context)
                throws IOException, InterruptedException {
            String rowkey = Bytes.toString(value.getRow());
            if (rowkey.length() != 16) {
                return;
            }
            //根据列族名和列名，分别取出城市编号、PM2.5的污染数值和数据时间
            String citycode = Bytes.toString(value.getValue(Bytes.toBytes(familyName), Bytes.toBytes("citycode")));
            String time = Bytes.toString(value.getValue(Bytes.toBytes(familyName), Bytes.toBytes("time")));
            String PM25 = Bytes.toString(value.getValue(Bytes.toBytes(familyName), Bytes.toBytes("PM25")));
            //城市编号+时间戳作为k，PM2.5的数据值作为value
            k.set(citycode + time);
            v.set(Integer.valueOf(PM25));
            //用context封装输出结果
            context.write(k, v);
            System.out.println("mapper:\t" + citycode + "\t" + time);
        }
    }
    
    //计算平均值
    public static class CityAvgReducer extends TableReducer<Text, IntWritable, ImmutableBytesWritable> {
    	
        @Override
        protected void reduce(Text key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {
            int sum = 0;
            int count = 0;
            //通过for循环取出集合中的每一个value值，过滤掉小于1的数据
            for (IntWritable value : values) {
                if (value.get() < 1) {
                    continue;
                }
                sum += value.get();
                count++;
            }
            //计算平均值，四舍五入取整
            int result = (int) ((float) sum / count + 0.5);
            //通过put来构建即将存入hbase的一条数据，将输入的k作为put对象的rowkey（6位的城市编号+10位的时间戳）
            Put put = new Put(Bytes.toBytes(key.toString()));
            //根据列族名、列名和列值来封装一条数据，设置3列分别是城市编号、PM2.5平均值和数据时间。城市编号和数据时间在k的对应位中截取获得
            String citycode = key.toString().substring(0, 6);
            put.add(Bytes.toBytes(familyName), Bytes.toBytes("citycode"), Bytes.toBytes(citycode));
            Integer length = key.toString().length();
            String time = key.toString().substring(length - 10, length);
            put.add(Bytes.toBytes(familyName), Bytes.toBytes("time"), Bytes.toBytes(time));
            put.add(Bytes.toBytes(familyName), Bytes.toBytes("PM25"), Bytes.toBytes(String.valueOf(result)));
            //将输入的k值转化为rowkey的类型做为输出的k，将put做为输出的value，封装到context中
            context.write(new ImmutableBytesWritable(Bytes.toBytes(key.toString())), put);
            System.out.println("reducer:\t" + citycode + "\t" + result);
        }
    }
    
    //执行mapper和reduce
    public static boolean doMR(String timestamp) throws MasterNotRunningException, ZooKeeperConnectionException, IOException, ClassNotFoundException, InterruptedException {
        //添加配置项
    	configuration.set("fs.defaultFS", "hdfs://master:9000");
        configuration.set("hadoop.job.ugi", "root,root");
        //初始化输入输出表
        initTB(inTable, outTable);
        //新建一个Job对象，将这个服务类设置为主类
        Job job = new Job(configuration);
        job.setJarByClass(MapReduceService.class);
        //创建一个查询条件，查找出指定时间的国控站点数据
        Scan scan = new Scan();
        //过滤器提取的rowkey包含时间戳和字符T的字符串
        Filter filter = new RowFilter(CompareOp.EQUAL, new SubstringComparator(timestamp + "T"));
        scan.setFilter(filter);
        //分发mapper工作，参数分别表示为输入数据表的表名、查询条件、执行mapper工作的类、mapper的输出成员k的类型、mapper的输出成员value的类型、job的实例
        TableMapReduceUtil.initTableMapperJob(inTable, scan, CityDataMapper.class, Text.class, IntWritable.class, job, false);
        //分发reducer工作，输出数据表的表名、执行reducer工作的类、job实例
        TableMapReduceUtil.initTableReducerJob(outTable, CityAvgReducer.class, job, null, null, null, null, false);
        //等待工作的执行
        boolean res = job.waitForCompletion(true);
        return res;
    }
    
    //创建一个对外接口来间接调用doDM方法
    public boolean getCityData(String timestamp) throws MasterNotRunningException, ZooKeeperConnectionException, ClassNotFoundException, IOException, InterruptedException {
        boolean res = doMR(timestamp);
        return res;
    }
    
    //测试
    public static void main(String[] args) {
        boolean res;
        try {
            res = doMR("1578477600");
            if (res) {
                System.out.println("success");
            } else {
                System.out.println("error");
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
    }

    private static void initTB(String intable, String outtable) throws MasterNotRunningException, ZooKeeperConnectionException, IOException {
        // TODO Auto-generated method stub
    	//创建一个表管理
        HBaseAdmin admin = new HBaseAdmin(configuration);
        //如果输入表不存在，输出错误信息，并直接返回
        if (!admin.tableExists(intable)) {
            System.out.println("table is not exists");
            return;
        }
        //如果输出表不存在，则新建一个输出表
        if (!admin.tableExists(outtable)) {
            HTableDescriptor descriptor = new HTableDescriptor(outtable);
            HColumnDescriptor family = new HColumnDescriptor(familyName);
            descriptor.addFamily(family);
            admin.createTable(descriptor);
        }
    }
}