package com.cstor.aqdata.controller;
/*
 * 封装排行榜接口
 */
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.cstor.aqdata.mapper.MariadbMapper;
import com.cstor.aqdata.service.HBaseService;
import com.cstor.aqdata.service.RedisService;
import com.cstor.aqdata.util.DateUtil;
import com.cstor.aqdata.util.Result;

@RestController
@RequestMapping("/rank")
@CrossOrigin
public class RankController {

    @Autowired
    private MariadbMapper mariadbMapper;
    
    @Autowired
    private HBaseService hBaseService;
    
    @Autowired
    private RedisService redisService;
    
    @RequestMapping(value="/station", method=RequestMethod.GET)
    public Result station(String sortorder) {
        if (!sortorder.equals("asc") && !sortorder.equals("desc")) {
            return new Result(false, "sortorder should be asc or desc");
        }
        String time = DateUtil.pastTimeStr(new Date(), -60);
        List<String> rowkeys = mariadbMapper.getStationRowkeys(time);
        JSONArray jsonArray = hBaseService.queryStation(rowkeys);
        
        List<JSONObject> result = JSONObject.parseArray(JSONObject.toJSONString(jsonArray), JSONObject.class);
        if (sortorder.equals("asc")) {
            Collections.sort(result, new Comparator<JSONObject>() {

                @Override
                public int compare(JSONObject o1, JSONObject o2) {
                    // TODO Auto-generated method stub
                    Integer sort1 = Integer.parseInt(o1.getString("PM25"));
                    Integer sort2 = Integer.parseInt(o2.getString("PM25"));
                    return sort1.compareTo(sort2);
                }
            });
        } else {
            Collections.sort(result, new Comparator<JSONObject>() {

                @Override
                public int compare(JSONObject o1, JSONObject o2) {
                    // TODO Auto-generated method stub
                    Integer sort1 = Integer.parseInt(o1.getString("PM25"));
                    Integer sort2 = Integer.parseInt(o2.getString("PM25"));
                    return sort2.compareTo(sort1);
                }
            });
        }
        return new Result(true, result);
    }
    
    @RequestMapping(value="/city", method=RequestMethod.GET)
    public Result city(String sortorder) {
        if (!sortorder.equals("asc") && !sortorder.equals("desc")) {
            return new Result(false, "sortorder should be asc or desc");
        }
        if (sortorder.equals("asc")) {
            return new Result(true, redisService.get("cityasc"));
        } else {
            return new Result(true, redisService.get("citydesc"));
        }
    }
}
