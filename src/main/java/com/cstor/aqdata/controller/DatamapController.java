package com.cstor.aqdata.controller;
/*
 * 封装数据地图前端调用接口
 */
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.cstor.aqdata.mapper.MariadbMapper;
import com.cstor.aqdata.service.HBaseService;
import com.cstor.aqdata.util.DateUtil;
import com.cstor.aqdata.util.HttpClientUtil;
import com.cstor.aqdata.util.Result;

@RestController
//处理请求地址映射
@RequestMapping("/datamap")
//允许跨域访问
@CrossOrigin
public class DatamapController {

	//注入mariadbMapper、hBaseService
    @Autowired
    private MariadbMapper mariadbMapper;
    
    @Autowired
    private HBaseService hBaseService;
    
    //查询国控点的最新数据
    @RequestMapping(value="/newDataGKD", method=RequestMethod.GET)
    public Result newDataGKD() {
    	//根据当前时间往前推迟60分钟
        String time = DateUtil.pastTimeStr(new Date(), -60);
        //通过 mariadbMapper里的方法获取rowkey
        List<String> rowkeys = mariadbMapper.getStationRowkeys(time);
        //通过hBaseService里的方法根据rowkey查询我们所需要的数据
        JSONArray jsonArray = hBaseService.queryStation(rowkeys);
        return new Result(true, jsonArray);
    }
    
    //根据国控站点的编号查询这个站点的历史数据
    @RequestMapping(value="/historyDataGKD", method=RequestMethod.GET)
    public Result historyDataGKD(String stationcode) {
        List<String> rowkeys = new ArrayList<String>();
        //循环获取过去24小时的数据
        for (int i = -23; i < 1; i++) {
            String time = DateUtil.pastTimeStr(new Date(), -60 + i * 60);
            rowkeys.add(mariadbMapper.getStationRowkey(time, stationcode));
        }
        JSONArray jsonArray = hBaseService.queryStation(rowkeys);
        return new Result(true, jsonArray);
    }
    
    //查询自控点的最新数据
    @RequestMapping(value="/newDataZjd", method=RequestMethod.GET)
    public Result newDataZjd() {
        try {
            String param = "";
            String url = "http://envpro.cstor.cn/envproject/data/deviceLivedata";
            JSONObject result = HttpClientUtil.httpPost(url, param);
            if (result.getBooleanValue("resultFlag")) {
                return new Result(true, result.getJSONArray("data"));
            } else {
                return new Result(false, result.getString("message"));
            }
        } catch (Exception e) {
            // TODO: handle exception
            return new Result(false, e.getMessage());
        }
    }
    
    @RequestMapping(value="/historyDataZJD", method=RequestMethod.GET)
    public Result historyDataZJD(String devid) {
        try {
            String param = "type=hour&num=24&devicecode=" + devid;
            String url = "http://envpro.cstor.cn/envproject/data/timeNumForDataByDevice";
            JSONObject result = HttpClientUtil.httpPost(url, param);
            if (result.getBooleanValue("resultFlag")) {
                return new Result(true, result.getJSONArray("data"));
            } else {
                return new Result(false, result.getString("message"));
            }
        } catch (Exception e) {
            // TODO: handle exception
            return new Result(false, e.getMessage());
        }
    }
}
