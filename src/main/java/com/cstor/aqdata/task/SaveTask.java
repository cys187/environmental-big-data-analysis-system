package com.cstor.aqdata.task;
/*
 * 定时爬取我的PM2.5网站上每个小时的数据
 */
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.cstor.aqdata.jpa.StationJPA;
import com.cstor.aqdata.mapper.MariadbMapper;
import com.cstor.aqdata.model.Station;
import com.cstor.aqdata.model.StationData;
import com.cstor.aqdata.service.HBaseService;
import com.cstor.aqdata.service.MapReduceService;
import com.cstor.aqdata.service.RedisService;
import com.cstor.aqdata.util.DateUtil;
import com.opencsv.CSVParser;
import com.opencsv.CSVReader;

@Component
public class SaveTask {

    @Autowired
    private StationJPA stationJPA;

    @Autowired
    private HBaseService hBaseService;
    
    @Autowired
    private MapReduceService mrService;
    
    @Autowired
    private RedisService redisService;
    
    @Autowired
    private MariadbMapper mariadbMapper;

    //设置定时任务的周期20s每次
    @Scheduled(cron="0/20 * * * * ?")
    public void getData() {
    	//爬虫脚本所在文件夹的上一级目录
        String dirPath = "E:\\Pycharm\\Code\\aq_data";
        //执行python脚本的命令
        String cmd = String.format("python %s\\programe\\main.py", dirPath);
        try {
        	//执行命令
            Process process = Runtime.getRuntime().exec(cmd);
            //等待命令执行完成
            process.waitFor();
            //根据当前时间获取文件名
            String filetime = DateUtil.getFileTime(new Date());
            //爬虫脚本执行成功的绝对路径
            String filePath = String.format("%s\\data\\%s.csv", dirPath, filetime);
            //System.out.println(filePath);
            //判断文件是否存在，若不存在输出error，若存在则说明爬虫脚本执行成功了，调用save2HBase方法进行读取数据文件，注入到Hbase中
            File file = new File(filePath);
            if (!file.exists()) {
                System.out.println("error");
            } else {
                Integer count = save2HBase(file);
                System.out.println("success\t" + count);
            }
        } catch (IOException | InterruptedException | ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
    }
    
    //进行读取数据文件，注入到Hbase中
    private Integer save2HBase(File file) throws IOException, ParseException {
        List<StationData> stationDatas = new ArrayList<StationData>();
        //将文件转化为文件输入流
        FileInputStream fileInputStream = new FileInputStream(file);
        //将文件输入流转化为字节流
        DataInputStream dataInputStream = new DataInputStream(fileInputStream);
        //将字节输入流转化为字符流，编码方式为UTF-8
        InputStreamReader inputStreamReader = new InputStreamReader(dataInputStream, "utf-8");
        //使用CSVReader类工具，第二个参数为CSV的分隔符，第三个参数为字符引号，第四个参数字符转义，最后一个参数为忽略行数
        CSVReader csvReader = new CSVReader(inputStreamReader, CSVParser.DEFAULT_SEPARATOR, CSVParser.DEFAULT_QUOTE_CHARACTER, CSVParser.DEFAULT_ESCAPE_CHARACTER, 0);
        //创建一个字符串数组用来读取CSVReader读取的每一行数据
        String[] data;
        while ((data = csvReader.readNext()) != null) {
            if (data.length != 4) {
                continue;
            }
            StationData stationData = new StationData();
            String cityname = data[0];
            String stationname = data[1];
            String PM25 = data[2];
            String time = DateUtil.str2Stamp(data[3]);
            //获取站点信息
            Station station = stationJPA.findByNameAndCityname(stationname, cityname);
            if (station == null) {
                continue;
            }
            //根据站点编号和时间戳来拼装RowKey
            String stationcode = station.getCode();
            String rowkey = stationcode + time + "T";
            stationData.setRowkey(rowkey);
            stationData.setStationcode(stationcode);
            stationData.setCitycode(station.getCitycode());
            stationData.setPM25(PM25);
            stationData.setTime(time);
            stationDatas.add(stationData);
        }
        csvReader.close();
        //调用Hbase里的插入方法并返回输入条数
        Integer count = hBaseService.insertStation(stationDatas);
        return count;
    }
    
    @Scheduled(cron="0/20 * * * * ?")
    public void save2Redis() {
        try {
            String time = DateUtil.pastTimeStr(DateUtil.getHourtime(new Date()), -60);
            List<String> rowkeysStation = mariadbMapper.getStationRowkeys(time);
            JSONArray stationDatas = hBaseService.queryStation(rowkeysStation);
            if (stationDatas != null && stationDatas.size() > 0) {
                String timestamp = DateUtil.str2Stamp(time);
                boolean res = mrService.getCityData(timestamp);
                if (res) {
                    List<String> rowkeys = mariadbMapper.getCityRowkeys(time);
                    List<JSONObject> result = hBaseService.queryCity(rowkeys);
                    if (result.size() > 0) {
                        Collections.sort(result, new Comparator<JSONObject>() {

                            @Override
                            public int compare(JSONObject o1, JSONObject o2) {
                                // TODO Auto-generated method stub
                                Integer sort1 = Integer.parseInt(o1.getString("PM25"));
                                Integer sort2 = Integer.parseInt(o2.getString("PM25"));
                                return sort1.compareTo(sort2);
                            }
                        });
                        redisService.set("cityasc", JSONObject.toJSONString(result));
                        Collections.sort(result, new Comparator<JSONObject>() {

                            @Override
                            public int compare(JSONObject o1, JSONObject o2) {
                                // TODO Auto-generated method stub
                                Integer sort1 = Integer.parseInt(o1.getString("PM25"));
                                Integer sort2 = Integer.parseInt(o2.getString("PM25"));
                                return sort2.compareTo(sort1);
                            }
                        });
                        redisService.set("citydesc", JSONObject.toJSONString(result));
                    }
                }
            } else {
                System.out.println("no stationdatas");
            }
        } catch (Exception e) {
            // TODO: handle exception
            System.out.println(e.getMessage());
        }
    }
}