package com.cstor.aqdata;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
//启用定时计划
@EnableScheduling
public class AqdataApplication {

	public static void main(String[] args) {
		SpringApplication.run(AqdataApplication.class, args);
	}

}
