package com.cstor.aqdata.util;
/*
 * 将接口数据封装成json格式
 */
public class Result {

    private boolean resultFlag = true; //结果标识
    private String message;   //结果消息
    private Object data;      //数据内容
    public boolean isResultFlag() {
        return resultFlag;
    }
    public void setResultFlag(boolean resultFlag) {
        this.resultFlag = resultFlag;
    }
    public String getMessage() {
        return message;
    }
    public void setMessage(String message) {
        this.message = message;
    }
    public Object getData() {
        return data;
    }
    public void setData(Object data) {
        this.data = data;
    }
    //创建构造方法
    public Result(boolean resultFlag, String message) {
        super();
        this.resultFlag = resultFlag;
        this.message = message;
    }
    public Result(boolean resultFlag, Object data) {
        super();
        this.resultFlag = resultFlag;
        this.data = data;
    }
    public Result(boolean resultFlag, String message, Object data) {
        super();
        this.resultFlag = resultFlag;
        this.message = message;
        this.data = data;
    }
}
