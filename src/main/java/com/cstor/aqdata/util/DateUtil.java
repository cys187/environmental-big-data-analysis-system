package com.cstor.aqdata.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtil {
	//添加字符串类型转时间类型公共方法
	public static Date str2Date(String str) throws ParseException {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(str);
    }
	//创建一个时间类型转字符串类型的方法
	public static String date2Str(Date date){
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date);
    }
	//将带时间的字符串转换为带时间戳
	public static String str2Stamp(String str) throws ParseException {
        Date date = str2Date(str);
        Long stamp = date.getTime() / 1000;
        return String.valueOf(stamp);
    }
	//将带时间戳转化为带时间格式
	public static String stamp2Str(String stamp) {
        Date date = new Date(new Long(stamp) * 1000);
        return date2Str(date);
    }
	//根据输入的date类型的时间和int类型的分钟数，获取时间前后几分钟的字符串
	 public static String pastTimeStr(Date date, int n) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.MINUTE, n);
        return date2Str(date);
	}
	//获取文件时间名
	 public static String getFileTime(Date date) {
        return new SimpleDateFormat("yyyyMMddHH").format(date);
	}
	//获取整点时间值
	 public static Date getHourtime(Date date) throws ParseException {
        String str = new SimpleDateFormat("yyyy-MM-dd HH:00:00").format(date);
        return new SimpleDateFormat("yyyy-MM-dd HH:00:00").parse(str);
	}
}
