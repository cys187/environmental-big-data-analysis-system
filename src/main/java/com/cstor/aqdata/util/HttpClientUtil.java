package com.cstor.aqdata.util;
import java.io.IOException;
import java.util.LinkedHashMap;

import org.apache.commons.httpclient.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.parser.Feature;
/*
 * http请求工具类
 */
public class HttpClientUtil {
	//创建配置
    private static RequestConfig requestConfig = null;
    //使用静态方法初始化请求和传输超时时间，设置为200s
    static {
        requestConfig = RequestConfig.custom().setSocketTimeout(200000).setConnectTimeout(200000).build();
    }
    //发送post请求，返回值为jsonobject类型，输入类型为url和字符串类型的请求参数
    public static JSONObject httpPost(String url, String param) throws ClientProtocolException, IOException {
        //编码方式为UTF-8
    	String code = "utf-8";
        CloseableHttpClient httpClient = HttpClients.createDefault();
        JSONObject jsonObject = new JSONObject();
        HttpPost httpPost = new HttpPost(url);
        httpPost.setConfig(requestConfig);
        if (param != null) {
            StringEntity entity = new StringEntity(param, code);
            entity.setContentEncoding(code);
            entity.setContentType("application/x-www-form-urlencoded");
            //设置post请求的请求头
            httpPost.setEntity(entity);
        }
        //发送请求获得响应
        CloseableHttpResponse response = httpClient.execute(httpPost);
        //解析响应
        if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
            String result = EntityUtils.toString(response.getEntity(), code);
            LinkedHashMap<String, Object> map = JSONObject.parseObject(result, LinkedHashMap.class, Feature.OrderedField);
            jsonObject.putAll(map);
        }
        return jsonObject;
    }
}