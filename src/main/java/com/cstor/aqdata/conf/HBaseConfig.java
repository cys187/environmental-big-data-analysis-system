package com.cstor.aqdata.conf;
//hbase配置类
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.client.HBaseAdmin;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;

@org.springframework.context.annotation.Configuration
public class HBaseConfig {
	//创建三个字符串类型的私有成员，从value注解从application.properties中获取相关配置项的值
	@Value("${hbase.zookeeper.quorum}")
	private String quorum; 
	
	@Value("${hbase.zookeeper.property.clientPort}")
	private String clientPort;  
	
	@Value("${zookeeper.znode.parent}")
	private String znodeParent;
	
	//创建Hadoop的Configuration实体类的构建方法
	@Bean
	public Configuration configuration(){
		//通过hbase的Configuration创建Configuration的对象
		Configuration configuration = HBaseConfiguration.create();
		//将三个私有成员set进这个对象中
		configuration.set("hbase.zookeeper.quorum", quorum);
		configuration.set("hbase.zookeeper.property.clientPort", clientPort);
		configuration.set("zookeeper.znode.parent",znodeParent);
		return configuration;
	}
	
	//创建Hbase接口的构建方法
	@Bean
	public Connection getConnection(){
		//初始化为Null，捕捉异常
		Connection connection = null;
		try {
			//通过Configuration配置创建Connection对象
			connection = ConnectionFactory.createConnection(configuration());
		} catch (Exception e) {
			//若捕捉异常，则输出错误信息
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
		return connection;
	}
	
	//创建HbaseAdmin实体类构建方法
	@Bean
	public HBaseAdmin getHBaseAdmin(){
		try {
			//通过Connection的方法建立连接
			Connection connection = getConnection();
			//返回HBaseAdmin的信息，捕捉异常
			return (HBaseAdmin) connection.getAdmin();
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			return null;
		}
		
	}
	//创建私有成员的getter和setter方法
	public String getQuorum() {
		return quorum;
	}

	public void setQuorum(String quorum) {
		this.quorum = quorum;
	}

	public String getClientPort() {
		return clientPort;
	}

	public void setClientPort(String clientPort) {
		this.clientPort = clientPort;
	}

	public String getZnodeParent() {
		return znodeParent;
	}

	public void setZnodeParent(String znodeParent) {
		this.znodeParent = znodeParent;
	}
	

}
