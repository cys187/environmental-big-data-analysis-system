package com.cstor.aqdata.mapper;
/*
 * 从远程数据库中，获取我们需要的数据
 */
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface MariadbMapper {
	
	//获取某时刻所有国控站点对应Rowkey的方法，并加上Select注解
    @Select("SELECT CONCAT(CODE, UNIX_TIMESTAMP(DATE_FORMAT(#{time}, '%Y-%m-%d %H:00:00')), 'T') FROM station")
    List<String> getStationRowkeys(@Param("time")String time);
    
    //根据时间和站点编号查询某时刻某个站点对应的Rowkey
    @Select("SELECT CONCAT(CODE, UNIX_TIMESTAMP(DATE_FORMAT(#{time}, '%Y-%m-%d %H:00:00')), 'T') FROM station WHERE CODE = #{stationcode}")
    String getStationRowkey(@Param("time")String time, @Param("stationcode")String stationcode);

    //获取某时刻所有城市站点对应Rowkey的方法
    @Select("SELECT DISTINCT CONCAT(citycode, UNIX_TIMESTAMP(DATE_FORMAT(#{time}, '%Y-%m-%d %H:00:00'))) FROM station")
    List<String> getCityRowkeys(@Param("time")String time);

    //根据城市编号查询城市名称
    @Select("SELECT DISTINCT cityname FROM station WHERE citycode = #{citycode}")
    String getCityname(@Param("citycode")String citycode);
}
