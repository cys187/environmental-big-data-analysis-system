package com.cstor.aqdata.jpa;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface BaseRepostory<T, PK extends Serializable> extends JpaRepository<T, PK> {

}
