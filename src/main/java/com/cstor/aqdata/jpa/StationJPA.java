package com.cstor.aqdata.jpa;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.cstor.aqdata.model.Station;
//StationJPA接口创建
public interface StationJPA extends BaseRepostory<Station, String>, JpaSpecificationExecutor<Station>, Serializable {
	Station findByNameAndCityname(String name, String cityname);
	
	Station findByCode(String code);
	
}
