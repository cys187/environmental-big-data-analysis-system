package com.cstor.aqdata.base;

import java.io.IOException;
import java.util.List;

import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.HBaseAdmin;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.Table;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
 
@Component //将普通组件实例化Spring容器当中
public class HBaseTemplate implements HBaseOperations {
	
	//创建私有成员，并加上Autowired注解，自动安装类型注入
    @Autowired
    private HBaseAdmin hBaseAdmin;

    @Autowired
    private Connection connection;

    //判断数据表是否存在
    @Override
    public boolean tableExists(String tableName) {
        boolean flag = false;
        Table table = null;
        try {
        	//通过connection获取该表名对应的数据表
            table = connection.getTable(TableName.valueOf(tableName));
            //通过hBaseAdmin判断数据表是否存在
            flag = hBaseAdmin.tableExists(table.getName());
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            System.out.println(e.getMessage());
        } finally {
            close(table); //关闭数据表
        }
        return flag;
    }
    
    //关闭数据表
    private void close(Table table) {
        // TODO Auto-generated method stub
        try {
            table.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
    }
    //生成数据表，输入参数为数据表名和列名
    @Override
    public void createTable(String tableName, String... familyName) {
    	 //判断数据表是否存在
        TableName tName = TableName.valueOf(tableName);
        //如果不存在，将列名循环添加进hbase的列描述符里
        if (!tableExists(tableName)) {
            HTableDescriptor hTableDescriptor = new HTableDescriptor(tName);
            for (int i = 0; i < familyName.length; i++) {
                HColumnDescriptor hColumnDescriptor = new HColumnDescriptor(familyName[i]);
                hTableDescriptor.addFamily(hColumnDescriptor);
            }
            try {
                hBaseAdmin.createTable(hTableDescriptor);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                System.out.println(e.getMessage());
            }
        } else {
            throw new IllegalArgumentException("table is already exists");
        }
    }
    
    //批量像数据表里插入数据，输入参数为数据表名和put类型的数组
    @Override
    public void putBatch(String tableName, List<Put> putList) {
        if (tableExists(tableName)) {
            Table table = null;
            try {
                table = connection.getTable(TableName.valueOf(tableName));
                table.put(putList);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } finally {
                close(table);
            }
        } else {
            throw new IllegalArgumentException("table is not exists");
        }
    }

    //创建自定义查询的方法，输入参数类型为字符串类型的表明，get类型的数组，返回值为Result[]数组
    @Override
    public Result[] query(String tableName, List<Get> getList) {
        if (!tableExists(tableName)) {
            return null;
        }
        Table table = null;
        Result[] results = null;
        try {
            table = connection.getTable(TableName.valueOf(tableName));
            results = table.get(getList);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            System.out.println(e.getMessage());
        } finally {
            close(table);
        }
        return results;
    }
}