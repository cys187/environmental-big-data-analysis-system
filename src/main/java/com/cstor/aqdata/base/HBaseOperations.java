package com.cstor.aqdata.base;
//创建HBaseOperations接口，用于封装Hbase的增、删、改、查操作命令
import java.util.List;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Result;

public interface HBaseOperations {

	boolean tableExists(String tableName);

	void createTable(String tableName, String[] familyName);

	void putBatch(String tableName, List<Put> putList);

	Result[] query(String tableName, List<Get> getList);
	
}
